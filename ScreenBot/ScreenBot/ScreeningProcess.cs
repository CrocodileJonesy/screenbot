﻿using System;
using System.Collections.Generic;


/// <summary>
/// Class to manage instances of a user's screening processe
/// </summary>
public class ScreeningProcess
{
    /// <summary>
    /// The amount of questions that will be asked
    /// </summary>
    private int m_questionCount;

    /// <summary>
    /// The user's name for the current screening process
    /// </summary>
    private string m_userName;

    /// <summary>
    /// The user's 4 digit number that appears in their profile info
    /// </summary>
    private string m_userDiscriminator;

    /// <summary>
    /// The user's ID for the current screening process
    /// </summary>
    private ulong m_userID;

    /// <summary>
    /// The current question that he user is on
    /// </summary>
    private int m_currentQuestion;

    /// <summary>
    /// Arrays to store both the chosen questions as well as the responses (Combined into one for each question)
    /// </summary>
    private string[] m_chosenQuestions, m_finalUserResponses;

    /// <summary>
    /// Array of user responses for a question before the user moves onto the next question
    /// </summary>
    private List<string> m_tempUserResponses;

    /// <summary>
    /// Has the user gotten through all of the questions
    /// </summary>
    private bool m_hasCompletedScreening;



    /// <summary>
    /// The user name of the recruit
    /// </summary>
    public string UserName { get { return m_userName; } }

    /// <summary>
    /// The user 4 digit code of the recruit
    /// </summary>
    public string UserTag { get { return m_userDiscriminator; } }

    /// <summary>
    /// The full user name (User and 4 digit code)
    /// </summary>
    public string FullUserName { get { return (m_userName + m_userDiscriminator); } }

    /// <summary>
    /// The user ID of the recruit
    /// </summary>
    public ulong UserID { get { return m_userID; } }

    /// <summary>
    /// The current question number that the screening is on
    /// </summary>
    public int CurrentQuestion { get { return m_currentQuestion + 1; } }

    /// <summary>
    /// Has all of the questions of the screening been answered
    /// </summary>
    public bool ScreeningCompleted { get { return m_hasCompletedScreening; } }

    /// <summary>
    /// Returns the current question (string) that the screening is on
    /// </summary>
    public string CurrentChosenQuestion
    {
        get
        {
            if (m_chosenQuestions[m_currentQuestion] == null || m_chosenQuestions[m_currentQuestion] == string.Empty)
            {
                m_chosenQuestions[m_currentQuestion] = ScriptEngineManager.scriptEngine.GetQuestion(m_currentQuestion, ScreeningManager.moduleLocation);
            }

            return m_chosenQuestions[m_currentQuestion];
        }
    }

    /// <summary>
    /// Default Constructor
    /// </summary>
    private ScreeningProcess()
    {
        m_userName = string.Empty;
        m_userID = 0;

        m_chosenQuestions = null;
        m_finalUserResponses = null;
    }

    /// <summary>
    /// Constructor for the screening manager (Essentially begins the screening process)
    /// </summary>
    /// <param name="recruitUserName">The discord username of the recruit</param>
    /// <param name="userTag">The 4 digit code of the user profile</param>
    /// <param name="recruitUserID">The ID of the discord user</param>
    public ScreeningProcess(string recruitUserName, string userTag, ulong recruitUserID)
    {
        m_userName = recruitUserName;
        m_userDiscriminator = userTag;
        m_userID = recruitUserID;
        m_currentQuestion = 0;
        m_hasCompletedScreening = false;
        m_questionCount = ScreeningManager.questionCount;

        m_chosenQuestions = new string[m_questionCount];
        m_finalUserResponses = new string[m_questionCount];
        m_tempUserResponses = new List<string>();
    }

    /// <summary>
    /// Constructor for the screening process
    /// </summary>
    /// <param name="recruitUserName">The discord name of the user</param>
    /// <param name="userTag">The 4 digit code of the user profile</param>
    /// <param name="recruitUserID">The ID of the user</param>
    /// <param name="questionCount">The number of questions for the user to ask</param>
    public ScreeningProcess(string recruitUserName, string userTag, ulong recruitUserID, int questionCount)
    {
        m_userName = recruitUserName;
        m_userDiscriminator = userTag;
        m_userID = recruitUserID;
        m_currentQuestion = 0;
        m_hasCompletedScreening = false;
        m_questionCount = questionCount;

        m_chosenQuestions = new string[questionCount];
        m_finalUserResponses = new string[questionCount];
        m_tempUserResponses = new List<string>();

    }

    /// <summary>
    /// Adds the generated question to the list
    /// </summary>
    /// <param name="question">The question to add</param>
    public void AddQuestion(string question)
    {
        if (m_chosenQuestions[m_currentQuestion] == null)
        {
            m_chosenQuestions[m_currentQuestion] = question;
        }
    }

    /// <summary>
    /// Adds a users response to the list of temp responses
    /// </summary>
    /// <param name="response"></param>
    public void AddResponse(string response)
    {
        if (m_tempUserResponses != null && !m_tempUserResponses.Contains(response))
        {
            m_tempUserResponses.Add(response);
        }
    }

    /// <summary>
    /// Finalises and combines the responses that the user has submmitted before moving to the next question
    /// Returns whether there have been any responses provided
    /// </summary>
    public bool CombineResponses()
    {
        // Delcare and intialise the final string (As an empty since we will combine later on)
        string combinedResponse = string.Empty;

        // Check if the user has provided a response at all
        if (m_tempUserResponses.Count < 1)
        {
            return false;
        }

        // Combine all of the user responses together (Seperated by new lines "/n")
        for (int i = 0; i < m_tempUserResponses.Count; ++i)
        {
            combinedResponse += m_tempUserResponses[i] + "\n";
        }

        // Add the combined responses to the array of user responses
        m_finalUserResponses[m_currentQuestion] = combinedResponse;

        // Clear the temp responses for the next question
        m_tempUserResponses.Clear();

        // Then move onto the next question
        ++m_currentQuestion;

        // Check if the questions have been answered
        if (m_currentQuestion >= m_questionCount)
        {
            // If they have then set the screening to true
            // as well as save the screening to disk
            m_hasCompletedScreening = true;
            SaveScreening();
        }

        return true;
    }

    /// <summary>
    /// Saves the screening to a file for review
    /// </summary>
    public void SaveScreening()
    {
        for (int i = 0; i < m_questionCount; ++i)
        {
            if (m_chosenQuestions[i] == null)
            {
                m_chosenQuestions[i] = string.Empty;
            }
            else
            {
                m_chosenQuestions[i] = ScriptEngineManager.scriptEngine.Cleaner(m_chosenQuestions[i]);
            }

            if (m_finalUserResponses[i] == null)
            {
                m_finalUserResponses[i] = string.Empty;
            }
            else
            {
                m_finalUserResponses[i] = ScriptEngineManager.scriptEngine.Cleaner(m_finalUserResponses[i]);
            }
        }

        m_userName = ScriptEngineManager.scriptEngine.Cleaner(m_userName);

        ScriptEngineManager.scriptEngine.SaveScreening(m_chosenQuestions, m_finalUserResponses, m_userName ,m_userID.ToString(), m_questionCount, ".//Completed Screenings//");
    }

    /// <summary>
    /// Sets the questions to the specified questions
    /// </summary>
    /// <param name="loadedQuestions">The questions that have been loaded</param>
    /// <param name="loadedResponses">The responses that have been loaded</param>
    public void LoadScreening(string[] loadedQuestions, string[] loadedResponses)
    {
        m_chosenQuestions = loadedQuestions;
        m_finalUserResponses = loadedResponses;

        for (int i = 0; i < m_questionCount; ++i)
        {
            // If both the question and the answer are not empty, then it is considered answered
            if (m_chosenQuestions[i] != null && m_finalUserResponses[i] != null)
            {
                ++m_currentQuestion;
            }

            //// If the next question and answer afterwards are both empty then this is the question we are after, and are done with the loop
            //if (m_chosenQuestions[i] == null && m_finalUserResponses[i] == null)
            //{
            //    ++m_currentQuestion;
            //    break;
            //}
        }

        if (m_chosenQuestions[m_currentQuestion] == null)
        {
            m_chosenQuestions[m_currentQuestion] = ScriptEngineManager.scriptEngine.GetQuestion(m_currentQuestion + 1, ScreeningManager.moduleLocation);
        }
    }

    /// <summary>
    /// Clears the screening and resets it back to the default state
    /// </summary>
    public void Clear()
    {
        // Clear all data stored
        m_tempUserResponses.Clear();
        System.Array.Clear(m_chosenQuestions, 0, m_chosenQuestions.Length);
        System.Array.Clear(m_finalUserResponses, 0, m_finalUserResponses.Length);

        // Set necessary values back to default state
        m_hasCompletedScreening = false;
        m_currentQuestion = 0;
    }

    /// <summary>
    /// Clears the screening and resets it back to the default state
    /// </summary>
    public void Reset()
    {
        Clear();
    }
}