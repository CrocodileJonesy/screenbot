from random import randint
import random

def random_numbers(number, maximum):
    numbers = random.sample(range(1,maximum),number+1)
    return numbers
        

def form(string):   #Removes or corrects character codes and syntax.
    corrected = string.replace("\n","").replace("\x93",'"').replace("\x94",'"')
    return corrected

def question_list(file_output):   #Collects and reformats question strings from .txt files into lists.
    question_list = []
    for line in file_output:
        question_list.append(form(line))
    return question_list

    
def standard_question(number, path = '.\\'):    #Collects and returns a standard question.
    std_raw = open(path + 'Question_' + str(number) + '.txt', 'r')    
    std_list = question_list(std_raw)
    std_out = std_list[0]
    return std_out

def random_question(number, path = '.\\'):  #Collects, selects and returns a randomised question.
    ran_raw = open(path + 'Question_' + str(number) + '.txt', 'r')    
    ran_list = question_list(ran_raw)
    ran_out = ran_list[randint(0, len(ran_list)-1)]
    return ran_out

def selection_question(number, topics = 3, path = '.\\'):   #Collects a randomaised selection question and selects a given number of randomised topics (3 by default) to go with it.
    sel_raw = open(path + 'Question_' + str(number) + '.txt', 'r')    
    sel_list = question_list(sel_raw)
    sel_start = sel_list[0]
    topic_indexs = random_numbers(topics, len(sel_list)-1)
    sel_end = ''
    for t in range(topics):
        if t == topics:
            pass
        elif t == topics-1:
            sel_end = sel_end + sel_list[topic_indexs[t]] + ' and ' + sel_list[topic_indexs[t+1]] + '.'
        else:
            sel_end = sel_end + sel_list[topic_indexs[t]] + ', '
    sel_out = sel_start + sel_end
    return sel_out

def question(number, path = '.\\'):   #Question generator function. Takes one int (the question number) and returns the corresponding question as a string.
    q_string = ''
    config_raw = open(path + 'Question_Config.txt', 'r')    
    config_list = question_list(config_raw)
    
    q_type = config_list[number-1]
    if q_type == 'standard' or q_type == 'Standard':
        q_string = standard_question(number, path)
    elif q_type == 'random' or q_type == 'Random':
        q_string = random_question(number, path)
    elif q_type > 8:
        topics = int(q_type[len(q_type)-1])-1
        q_string = selection_question(number, topics, path)
    else:
        print ('Error')
    
    return q_string
    
def question_number(path = '.\\'):
    config_raw = open(path + 'Question_Config.txt', 'r')    
    config_list = question_list(config_raw)
    number = len(config_list)
    return number


