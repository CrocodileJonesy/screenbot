def cleaner(item):
    PERMITTED_CHARS = " .0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ#(),:" 
    cleaned = "".join(c for c in item if c in PERMITTED_CHARS)
    return cleaned
