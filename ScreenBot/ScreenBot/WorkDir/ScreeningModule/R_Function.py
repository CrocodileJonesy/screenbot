def retrieve_screen(name, path = '.\\'):
    q_list = []
    q_str = ""
    r_list = []
    r_str = ""
    q_add = False
    r_add = False
    brack = 0
    line_count = 0
    line_pos = 0
    partial_screens = open(path + 'Incomplete_Screenings.txt', 'r')
    for line in partial_screens:
        comp = ""
        line_count += 1
        if line_count == 0:
            pass
        else:
            line = line.replace("\n","")
            list_line = list(line)
            for pos in range(len(name)):
                comp = comp + list_line[pos]
            if comp == name:
                line_pos = line_count
                for i in list_line:
                    if i == ']':
                        if brack == 1:
                            q_add = False
                        elif brack == 2:
                            r_add = False
                    if q_add == True:
                        q_str = q_str + i
                    elif r_add == True:
                        r_str = r_str + i
                    if i == '[':
                        if brack == 0:
                            brack = 1
                            q_add = True
                        elif brack == 1:
                            brack = 2
                            r_add = True
                list_q_str = q_str.split("'")
                for i in list_q_str:
                    if len(i) > 2:
                        q_list.append(str(i))
                list_r_str = r_str.split("'")
                for i in list_r_str:
                    if len(i) > 2:
                        r_list.append(str(i))
    partial_screens.close()
    f = open(path +"Incomplete_Screenings.txt","r+")
    d = f.readlines()
    f.seek(0)
    count = 0
    for i in d:
        count += 1
        if count != line_pos:
            f.write(i)
    f.truncate()
    f.close()
    return [q_list, r_list]




