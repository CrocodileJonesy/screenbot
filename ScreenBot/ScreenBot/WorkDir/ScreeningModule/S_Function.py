def form(array):
    fin_array = []
    string_array = str(array)
    string_array = string_array.replace("Array[str]","").replace("((",",").replace("))","").replace(",","")
    list_array = string_array.split("'")
    for i in list_array:
        if len(i) > 1:
            fin_array.append(str(i))
    return fin_array
    
def screen_store(questions, responces, name, id, questionCount ,path = '.\\'):
    q_list = form(questions)
    r_list = form(responces)
    if len(q_list) < questionCount:
        incomplete = open(path + 'Incomplete_Screenings.txt', 'a')
        incomplete.write(id + str(q_list) + str(r_list) + "\n")
        incomplete.close()
    else:
        complete = open(path + 'Completed Screenings//' + name + '_screening.txt', 'a')
        complete.write("\n" + "" + "\n" + "==================================================")
        complete.write("\n" + "Completed screening for: " + name + "\n" + "-----------------------")
        pos = 0
        for i in q_list:
            q = ""
            r = ""
            count = 1
            for char in q_list[pos]:
                if count % 100 == 0:
                    q = q + char + "\n"
                else:
                    q = q + char
                count += 1
            
            count = 1
            for char in r_list[pos]:
                if count % 100 == 0:
                    r = r + char + "\n"
                else:
                    r = r + char
                count += 1
                
            
            complete.write("\n" + q + "\n" + "")
            complete.write("\n" + r + "\n" + "" + "\n" + "")
            pos += 1
        complete.close()


