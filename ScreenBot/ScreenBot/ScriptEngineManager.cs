﻿using System.Collections.Generic;

using IronPython.Hosting;
using IronPython.Runtime;
using Microsoft.Scripting.Hosting;
using Microsoft.Extensions.DependencyInjection;

/// <summary>
/// Singleton that manages the loading and use of IronPython libraries
/// </summary>
public class ScriptEngineManager
{
    /// <summary>
    /// Singleton for the script manager
    /// </summary>
    public static ScriptEngineManager scriptEngine;

    /// <summary>
    /// Engine for reading python
    /// </summary>
    private ScriptEngine engine;

    /// <summary>
    /// The number of questions recieved from a python script)
    /// </summary>
    private dynamic m_getQuestionCount;

    /// <summary>
    /// The question function (Recieved from a python script)
    /// </summary>
    private dynamic m_getQuestion;

    /// <summary>
    /// The screen store function (Recieved from a python script)
    /// </summary>
    private dynamic m_screenStore;

    /// <summary>
    /// Loads and reads any incomplete screenings (Recieved from a python script)
    /// </summary>
    private dynamic m_retrieveScreening;

    /// <summary>
    /// Cleans and organises strings (Recieved from a python script)
    /// </summary>
    private dynamic m_cleaner;



    /// <summary>
    /// Constructor for the singelton
    /// </summary>
    private ScriptEngineManager(string searchPath)
    {
        engine = Python.CreateEngine();
        ScriptScope scope = engine.CreateScope();

        ScriptScope qFunction = engine.CreateScope(); // Temp scope for Q_Function.py
        ScriptScope sFunction = engine.CreateScope(); // Temp scope for S_Function.py
        ScriptScope rFunction = engine.CreateScope(); // Temp scope for R_Function.py
        ScriptScope cFunction = engine.CreateScope(); // Temp scope for C_Function.py

        string path = ".\\ScreeningModule\\";

        var searchPaths = engine.GetSearchPaths();
        //searchPaths.Add(@"C:\Program Files (x86)\IronPython 2.7\Lib"); // This a temp solution, Will need to sort out
        searchPaths.Add(@".\PyLib");
        engine.SetSearchPaths(searchPaths);

        engine.ExecuteFile(path + @"Q_Function.py", scope);
        m_getQuestion = scope.GetVariable("question");
        m_getQuestionCount = scope.GetVariable("question_number");

        scope = engine.CreateScope();
        engine.ExecuteFile(path + @"S_Function.py", scope);
        m_screenStore = scope.GetVariable("screen_store");

        scope = engine.CreateScope();
        engine.ExecuteFile(path + @"R_function.py", scope);
        m_retrieveScreening = scope.GetVariable("retrieve_screen");

        scope = engine.CreateScope();
        engine.ExecuteFile(path + @"C_Function.py", scope);
        m_cleaner = scope.GetVariable("cleaner");

    }

    /// <summary>
    /// Creates, and returns, an instance of the ScriptEngineManager
    /// </summary>
    /// <returns></returns>
    public static ScriptEngineManager CreateInstance(string searchPath = ".\\ScreningModule\\")
    {
        if (scriptEngine == null)
        {
            scriptEngine = new ScriptEngineManager(searchPath);
        }

        return scriptEngine;
    }

    /// <summary>
    /// Wrapper function for getting the question count
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public int GetQuestionCount(string path = ".\\")
    {
        return m_getQuestionCount(path);
    }

    /// <summary>
    /// Wrapper function for question function (from py script)
    /// </summary>
    /// <param name="question"></param>
    /// <returns></returns>
    public dynamic GetQuestion(int number, string path = ".\\")
    {
        return m_getQuestion(number, path);
    }

    /// <summary>
    /// Wrapper function for writing the screening results (Whether completed or not) to a file (from py script)
    /// </summary>
    /// <param name="questions">The list of questions stored</param>
    /// <param name="responses">The user's reponses stored</param>
    /// <param name="name">The user name</param>
    /// <param name="path">The path to which to write the files to</param>
    public void SaveScreening(string[] questions, string[] responses, string name, string id, int questionCount, string path = ".\\")
    {
        m_screenStore(questions, responses, name, id, questionCount, path);
    }

    /// <summary>
    /// Wrapper for retrieving incomplete screening results that have been saved
    /// </summary>
    /// <param name="userName">The username in which to load the screening for</param>
    /// <param name="path">The location of the incomplete screenings</param>
    /// <returns>2D string array</returns>
    public string[][] RetrieveScreening(string userName, string path = ".\\")
    {
        IList<object> list = m_retrieveScreening(userName, path);

        string[][] retrievedData = new string[2][];

        dynamic gottenQuestions = list[0];
        dynamic gottenResponses = list[1];

        int questionsCount = ((IList<object>)gottenQuestions).Count;
        int responsesCount = ((IList<object>)gottenResponses).Count;

        retrievedData[0] = new string[ScreeningManager.questionCount];
        retrievedData[1] = new string[ScreeningManager.questionCount];

        for (int i = 0; i < questionsCount; ++i)
        {
            retrievedData[0][i] = gottenQuestions[i] as string;
        }

        for (int i = 0; i < responsesCount; ++i)
        {
            retrievedData[1][i] = gottenResponses[i] as string;
        }

        return retrievedData;
    }

    /// <summary>
    /// Wrapper function for cleaning and organising (And returns a string) a passed in string to clean up
    /// </summary>
    /// <param name="stringToClean">the string to be checked and cleaned</param>
    /// <returns>string</returns>
    public string Cleaner(string stringToClean)
    {
        return m_cleaner(stringToClean);
    }
}