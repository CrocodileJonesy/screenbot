﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;
using Discord.Commands;

/// <summary>
/// Singleton that will manage the screening process (Singleton moreso intended as a bridge between main program and screening module).
/// This is also the central class for manage of anything relating to the bot and screenings
/// </summary>
public class ScreeningManager
{
    /// <summary>
    /// The screening manager
    /// </summary>
    public static ScreeningManager screenMngr;


    /// <summary>
    /// The amount of questions for the arrays of questions and reponses
    /// </summary>
    public static int questionCount;

    /// <summary>
    /// The location of a particular module to use
    /// </summary>
    public static string moduleLocation;

    /// <summary>
    /// The token of the bot that the program will be using
    /// </summary>
    public string botToken = string.Empty;

    /// <summary>
    /// The channel that the bot will post the screening to
    /// </summary>
    public ulong screeningsChannel = 0;

    /// <summary>
    /// The string that will store the message that will be sent to a new joining user
    /// </summary>
    public string userJoinMessage;

    /// <summary>
    /// The message that will be displayed when a screening is completed
    /// </summary>
    public string screenCompleteMessage;



    /// <summary>
    /// The current list of screenings that are in progress
    /// </summary>
    private List<ScreeningProcess> m_screenings;



    /// <summary>
    /// Gets the list of the current screenings
    /// </summary>
    public int ScreeningsListCount { get { return m_screenings.Count; } }



    /// <summary>
    /// Constructor for screening manager (CreateInstance must be called to use this)
    /// </summary>
    private ScreeningManager()
    {
        m_screenings = new List<ScreeningProcess>(); // Initialise lists
        LoadConfig(); // Load the necessary data from the config file
        ScriptEngineManager.CreateInstance(moduleLocation); // Create the ScriptEngineManager
        questionCount = ScriptEngineManager.scriptEngine.GetQuestionCount(moduleLocation); // Get the question count
        userJoinMessage = string.Format(userJoinMessage, questionCount); // Format the message

        // Check to see if the incomplete_screenings file exists, if not then create one, and close as we have no use for it right away
        FileStream incompletesCheck = new FileStream("Incomplete_Screenings.txt", FileMode.OpenOrCreate);
        incompletesCheck.Dispose();
    }

    /// <summary>
    /// Initialises the singleton (And returns it if already intialised)
    /// </summary>
    public static ScreeningManager CreateInstance()
    {
        // Check if the singelton hasn't already been intitialised
        if (screenMngr == null)
        {
            screenMngr = new ScreeningManager();
        }

        // Return the screening
        return screenMngr;
    }

    /// <summary>
    /// Add an instance of the screening process to the list of screenings
    /// </summary>
    /// <param name="creenProcess">The screening to add</param>
    public void AddScreening(string userName, string userdiscriminator, ulong userID)
    {
        ScreeningProcess newScreening = new ScreeningProcess(userName, userdiscriminator, userID, questionCount);

        // Check if the instance of the screening process is not already added
        if (!m_screenings.Contains(newScreening))
        {
            // Add the screening to the list
            m_screenings.Add(newScreening);
        }
    }

    /// <summary>
    /// Adds an instance of the screening process to the list
    /// </summary>
    /// <param name="screeningToAdd">The screening to add</param>
    public void AddScreening(ScreeningProcess screeningToAdd)
    {
        // Check if the screening already exists
        if (!m_screenings.Contains(screeningToAdd))
        {
            // Add it to the screening
            m_screenings.Add(screeningToAdd);
        }
    }

    /// <summary>
    /// Removes an instance of a screening from the list
    /// </summary>
    /// <param name="screenProcess">The screening to remove</param>
    public void RemoveScreening(ScreeningProcess screenProcess)
    {
        // Check if the screening is already in the list
        if (m_screenings.Contains(screenProcess))
        {
            // Remove the screening, if so
            m_screenings.Remove(screenProcess);
        }
        else // Otherwise just return
        {
            return;
        }
    }

    /// <summary>
    /// Gets a screening based on the specified user ID. Returns null if not found
    /// </summary>
    /// <param name="userID">The user ID to get the screening of</param>
    /// <returns></returns>
    public ScreeningProcess GetScreening(ulong userID)
    {
        for (int i = 0; i < m_screenings.Count; ++i)
        {
            if (m_screenings[i].UserID == userID) // Check if the user has a screening
            {
                // Then return the screening
                return m_screenings[i];
            }
        }

        // Return null if the screening was not found
        return null;
    }

    /// <summary>
    /// Removes a user's screening from the list
    /// </summary>
    /// <param name="userID">The user's ID to remove the screening</param>
    public void RemoveScreening(ulong userID)
    {
        for (int i = 0; i < m_screenings.Count; ++i)
        {
            if (m_screenings[i].UserID == userID) // Check if the user already has a screening
            {
                // Then remove the screening and return
                m_screenings.RemoveAt(i);
                return;
            }
        }
    }

    /// <summary>
    /// Saves all current open screenings
    /// </summary>
    public void SaveAllScreenings()
    {
        for (int i = 0; i < m_screenings.Count; ++i)
        {
            m_screenings[i].SaveScreening();
        }
    }

    /// <summary>
    /// Checks if a user has any previously saved data
    /// </summary>
    /// <param name="userName">The username to load the data from</param>
    /// <param name="userID">The users ID to pass in</param>
    /// <param name="screening">The screening that it sent back</param>
    /// <returns></returns>
    public bool LoadScreening(ref ScreeningProcess screening)
    {
        string[][] returnedResults = ScriptEngineManager.scriptEngine.RetrieveScreening(screening.UserID.ToString());

        string[] foundQuestions = returnedResults[0];
        string[] foundResponses = returnedResults[1];

        // Check if data was found, if not then return false
        if (foundQuestions[0] == null && foundResponses[0] == null)
        {
            return false;
        }

        // load the screening entry with the data loaded from file
        // and then add to the list of screenings
        screening.LoadScreening(foundQuestions, foundResponses);
        AddScreening(screening);

        // Return true if all went correctly
        return true;
    }

    /// <summary>
    /// Loads the config file
    /// </summary>
    public void LoadConfig()
    {
        string line = string.Empty;

        try
        {
            // Get the config file
            StreamReader configFile = new StreamReader(".\\config.ini");

            // Read the first line
            line = configFile.ReadLine();

            // Refernce array to split string with, once read
            char[] splitLines = { '=' };

            // If the first line conatins config, then it is the file we're looking for
            if (line.Contains("Config"))
            {
                // Loop through the file and read through the lines a parse accordingly
                while (line != null)
                {
                    // varaible to store read in lines
                    string[] lines = line.Split(splitLines);

                    // Run through the file and read in the lines here
                    if (line.Contains("botToken")) // Read in the bot that will be connecting to
                    {
                        botToken = lines[1];
                        botToken = botToken.Replace(" ", string.Empty);
                    }
                    else if (line.Contains("screeningChannel")) // Read in the channel ID the bot will post to
                    {
                        screeningsChannel = ulong.Parse(lines[1]);
                    }
                    else if (line.Contains("modulePath")) // Read in the number of questions that the bot will ask
                    {
                        moduleLocation = ".\\" + lines[1] + "\\";
                        moduleLocation = moduleLocation.Replace(" ", string.Empty);
                    }
                    else if (line.Contains("userJoinMessage")) // Read in the message that will be sent to a new joining user
                    {
                        line = configFile.ReadLine();

                        if (line.Equals("{"))
                        {
                            line = configFile.ReadLine();
                            userJoinMessage = string.Empty;

                            while (!line.Equals("}"))
                            {
                                line = line.Remove(0, 4);
                                userJoinMessage += line + '\n';
                                line = configFile.ReadLine();
                            }
                        }
                    }
                    else if (line.Contains("screenCompleteMessage")) // Read in the message that will display when the user completes the screening
                    {
                        line = configFile.ReadLine();

                        if (line.Equals("{"))
                        {
                            line = configFile.ReadLine();
                            screenCompleteMessage = string.Empty;

                            while (!line.Equals("}"))
                            {
                                line = line.Remove(0, 4);
                                screenCompleteMessage += line + '\n';
                                line = configFile.ReadLine();
                            }
                        }
                    }

                    line = configFile.ReadLine();
                }
            }
        }
        catch (Exception e) // Handle the error of the file not being found (By printing a message in the console and then exiting)
        {
            if (e.InnerException.GetType() == typeof(IOException))
            {
                Console.WriteLine("config.ini not found, program will now exit");
            }
            else
            {
                Console.WriteLine("An error occured (" + e.Message + ")");
            }

            Thread.Sleep(1000);
            System.Environment.Exit(1);
        }
    }

    /// <summary>
    /// Posts a completed user screening
    /// </summary>
    /// <param name="context">The current user</param>
    /// <param name="channelID">The channel to post the screenings to</param>
    /// <param name="screeningPath">The location of the completed screening</param>
    public void PostScreening(SocketCommandContext context, ulong channelID, string screeningPath, string message)
    {
        // If a channel to post to has been configured
        // otherwise there is no channel to post to
        if (channelID != 0)
        {
            // Post the completed screening to the channel
            ((SocketTextChannel)context.Client.GetChannel(channelID)).SendFileAsync(screeningPath, message);
        }
        else
        {
            Console.WriteLine("No channel configured for posting screenings");
        }
    }
}
