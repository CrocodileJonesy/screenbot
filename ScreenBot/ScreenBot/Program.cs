﻿using Discord;
using Discord.WebSocket;
using Discord.Net.WebSockets;
using Discord.Net.Udp;
using Discord.Commands;

using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using System.Reflection;
using System.Diagnostics;
using System.ComponentModel;

using Discord.Net.Providers.WS4Net;
using Discord.Net.Providers.UDPClient;

// TODO: 

namespace ScreenBot
{
    public class Program
    {
        /// <summary>
        /// Discord bot client
        /// </summary>
        private DiscordSocketClient client;

        /// <summary>
        /// Command service to manage bot comands
        /// </summary>
        private CommandService commands;

        #region Trap app exit
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        // On application exit handler
        private static bool Handler(CtrlType sig)
        {
            if (ScreeningManager.screenMngr.ScreeningsListCount > 0)
            {
                // Save all of the current screenings
                ScreeningManager.screenMngr.SaveAllScreenings();

                Console.WriteLine("Saving screenings to file");

                // Wait for screenings to finish saving
                Thread.Sleep(ScreeningManager.screenMngr.ScreeningsListCount * 500);
            }

            // Then exit
            Environment.Exit(-1);
            return true;
        }
        #endregion

        public static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        // The main function that gets called when the program starts
        public async Task MainAsync()
        {
            // Set up on exit event
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            // Initialise the singleton
            ScreeningManager.CreateInstance();

            client = new DiscordSocketClient(new DiscordSocketConfig
            {
                WebSocketProvider = WS4NetProvider.Instance,
                UdpSocketProvider = UDPClientProvider.Instance,
            });

            commands = new CommandService();
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());

            client.Log += Log;
    
            string token = ScreeningManager.screenMngr.botToken;

            client.MessageReceived += MessageRecieved;
            client.UserJoined += UserJoined;
            client.UserLeft += UserLeft;
            //client.LoggedOut += LoggedOut;

            await client.LoginAsync(TokenType.Bot, token); // Login to the bot account (With the token)
            await client.StartAsync();

            // Set the title to make clearer the name, and version
            Console.Title = "Screening Bot v1.13";

            await Task.Delay(-1);
        }

        // This gets called each time a message is sent to a channel (or DM) that the bot can see
        private async Task MessageRecieved(SocketMessage message)
        {
            var msg = message as SocketUserMessage;
            if (msg == null)
                return;

            var context = new SocketCommandContext(client, msg);
            int argPos = 0;

            // Check if the user has issued a command or just typed in a regular message
            if (msg.HasCharPrefix('?', ref argPos))
            {
                var result = await commands.ExecuteAsync(context, argPos);

                if (!result.IsSuccess && result.Error != CommandError.UnknownCommand)
                {
                    Console.WriteLine("An error was encountered, error code: " + result.ErrorReason);
                    await context.Channel.SendMessageAsync("An error has been enountered, please contact the admins and include this message: " + result.ErrorReason);
                }
            }
            else
            {
                ScreeningProcess screening = ScreeningManager.screenMngr.GetScreening(context.User.Id); // Get the screening

                // Check if the user has begun their screening as well as the screening has not been completed
                if (screening != null && !screening.ScreeningCompleted)
                {
                    if (message.Content != string.Empty && message.Content.Length >= 3) // Check if the message is valid/not empty
                    {
                        screening.AddResponse(message.Content); // Add the user's response to the list
                    }
                }
            }
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());

            return Task.CompletedTask;
        }

        // This gets called each time a new user joins
        private async Task UserJoined(SocketGuildUser arg)
        {
            Console.WriteLine(arg.Username + "#" + arg.Discriminator + " has joined");

            if (true)
            {
                await arg.SendMessageAsync(ScreeningManager.screenMngr.userJoinMessage);
            }
        }

        private async Task UserLeft(SocketGuildUser arg)
        {
            Console.WriteLine("user " + arg.Username + "#" + arg.Discriminator + " left");
            await Task.CompletedTask;
        }

        //private async Task LoggedOut(SocketGuildUser arg)
        //{
        //    ScreeningProcess userScreening = ScreeningManager.screenMngr.GetScreening(arg.Id);

        //    // Check if the user has a currently active screening
        //    if(userScreening != null)
        //    {
        //        userScreening.SaveScreening();
        //        Console.WriteLine("user " + arg.Username + " logged off, saving screening");
        //    }

        //    await Task.CompletedTask;
        //}

        //private async Task HandleCommandAsync(SocketMessage arg)
        //{
        //    var message = arg as SocketUserMessage;

        //    if (message == null || message.Author.IsBot) { return; }

        //    int argPos = 0;

        //    // TODO:
        //}

        //private async Task Disconnected(SocketMessage message)
        //{

        //}



        #region PythonEngine code
        //    //ProcessStartInfo start = new ProcessStartInfo();
        //    //start.FileName = ".\\TestModule1\\Test 2.exe";
        //    ////start.Arguments = string.Format("{0}", args);
        //    //start.UseShellExecute = false;
        //    //start.RedirectStandardOutput = true;

        //    //using (Process process = Process.Start(start))
        //    //{
        //    //    using (StreamReader reader = process.StandardOutput)
        //    //    {
        //    //        string result = reader.ReadToEnd();
        //    //        Console.WriteLine("Test Module 1");
        //    //        Console.WriteLine(result);
        //    //    }
        //    //}

        //    //ProcessStartInfo module2 = new ProcessStartInfo(".\\TestModule2\\Test 3.exe");
        //    //module2.UseShellExecute = false;
        //    //module2.RedirectStandardOutput = true;

        //    //using (Process process = Process.Start(module2))
        //    //{
        //    //    using (StreamReader reader = process.StandardOutput)
        //    //    {
        //    //        string result = reader.ReadToEnd();
        //    //        Console.WriteLine("Test Module 2");
        //    //        Console.WriteLine(result);
        //    //    }
        //    //}

        //    //ProcessStartInfo module3 = new ProcessStartInfo(".\\TestModule3\\Test 4.exe");
        //    //module3.UseShellExecute = false;
        //    //module3.RedirectStandardOutput = true;

        //    //using (Process process = Process.Start(module3))
        //    //{
        //    //    using (StreamReader reader = process.StandardOutput)
        //    //    {
        //    //        string result = reader.ReadToEnd();
        //    //        Console.WriteLine("Test Module 3");
        //    //        Console.WriteLine(result);
        //    //    }
        //    //}

        //    //var pySrc = @"def CalcAdd(Numb1, Numb2):
        //    //            return Numb1 + Numb2";

        //    ScriptEngine engine = Python.CreateEngine();
        //    ScriptScope scope = engine.CreateScope();
        //    //ObjectOperations ops = engine.Operations;

        //    engine.ExecuteFile(@"Test_3.py", scope);

        //    var getMult = scope.GetVariable("Multiply");

        //    var multiply = getMult(4, 4);
        //    var mult2 = getMult(5, 5);


        //    engine.ExecuteFile(@"Test_4.py", scope);

        //    var sqrRoot = scope.GetVariable("Square_Root");
        //    var sqrRootResult = sqrRoot(5);

        //    Console.WriteLine(sqrRootResult);

        //    //Console.WriteLine("Test_3: ", multiply);

        //    //engine.Execute(pySrc, scope);


        //    //var getCalc = scope.GetVariable("CalcAdd");
        //    //var resultAdd = getCalc(1917, 1);

        //    //Console.WriteLine("GetCalc: " + resultAdd);

        //    //engine.ExecuteFile("PrintTest.py");
        #endregion
    }


    public class ScreeningModule : ModuleBase<SocketCommandContext>
    {

        #region TestCommands
        //[Command("ping")]
        //public async Task PingAsync()
        //{
        //    //await Context.Channel.SendMessageAsync("pong!");
        //    await ReplyAsync("pong!");
        //    //await ReplyAsync(echo);
        //}

        //[Command("test")]
        //public async Task TestAsync()
        //{
        //    //await ReplyAsync("Testing");
        //    await Context.User.SendMessageAsync("Testing");
        //}

        //[Command("testq")]
        //public async Task TestScreening([Summary("The question to pick")] int question)
        //{
        //    //var channelName = Context.Client.GetChannel(387754881810694145);
        //    InitScriptEngine();

        //    engine.ExecuteFile(@"Q_Function.py", scriptScope);

        //    var GetQuestion = scriptScope.GetVariable("question");

        //    await Context.Channel.SendMessageAsync((string)GetQuestion(question));
        //}

        //[Command("TestList")]
        //public async Task TestList()
        //{
        //    InitScriptEngine();

        //    engine.ExecuteFile(@"Listalt.py", scriptScope);

        //    var list_function = scriptScope.GetVariable("list_function");

        //    string[,] stringArr = new string[5, 7];
        //    int[] stringArr2 = new int[5];



        //    await list_function(stringArr, stringArr2);
        //}
        #endregion


        /// <summary>
        /// Bot command to begin the screening process
        /// </summary>
        /// <returns></returns>
        [Command("begin")]
        public async Task BeginScreening()
        {
            ScreeningProcess screening = ScreeningManager.screenMngr.GetScreening(Context.User.Id);

            if (screening == null)
            {
                screening = new ScreeningProcess(Context.User.Username, "#" + Context.User.Discriminator, Context.User.Id);

                if (ScreeningManager.screenMngr.LoadScreening(ref screening))
                {
                    Console.WriteLine("Found previous screening for user " + screening.FullUserName);
                    string currentQuestion = screening.CurrentChosenQuestion;
                    await Context.User.SendMessageAsync((screening.CurrentQuestion) + ". " + currentQuestion);
                    return;
                }
                else
                {
                    screening = null;
                }
            }

            if (screening != null)
            {
                await Context.User.SendMessageAsync("You have already begun your screening");
                return;
            }


            ScreeningManager.screenMngr.AddScreening(Context.User.Username, "#" + Context.User.Discriminator, Context.User.Id);
            screening = ScreeningManager.screenMngr.GetScreening(Context.User.Id);

            string question = ScriptEngineManager.scriptEngine.GetQuestion(screening.CurrentQuestion, ScreeningManager.moduleLocation);

            screening.AddQuestion(question);

            Console.WriteLine("User " + Context.User.Username + "#" + Context.User.Discriminator + " began screening");

            await Context.User.SendMessageAsync((screening.CurrentQuestion) + ". " + question);
        }

        /// <summary>
        /// Bot command to move onto the next question
        /// </summary>
        /// <returns></returns>
        [Command("next")]
        public async Task NextQuestion()
        {
            ScreeningProcess screening = ScreeningManager.screenMngr.GetScreening(Context.User.Id); // Get the screening of the current user

            // Check if the user has begun their screening
            if (screening == null)
            {
                screening = new ScreeningProcess(Context.User.Username, "#" + Context.User.Discriminator, Context.User.Id);

                // Attempt to find, and load, any previous data of the user
                // If so then continue from where was left off otherwise user has not begun their screening
                if (ScreeningManager.screenMngr.LoadScreening(ref screening))
                {
                    Console.WriteLine("Loaded previous screening for user " + screening.FullUserName);
                    string question = screening.CurrentChosenQuestion;
                    await Context.User.SendMessageAsync((screening.CurrentQuestion) + ". " + question);
                    return;
                }
                else
                {
                    await Context.User.SendMessageAsync("You have not begun your screening. Please type in '?begin' to start your screening");
                    return;
                }
            }

            // Check if the user has completed their screening
            if (screening.ScreeningCompleted)
            {
                await Context.User.SendMessageAsync("You have already answered all of the questions");
                return;
            }

            // Check if the screening is null
            if (screening != null && screening.CurrentQuestion <= ScreeningManager.questionCount)
            {
                // Output to the console that the user has answered the question
                Console.WriteLine(Context.User.Username + "#" + Context.User.Discriminator + " answered question " + screening.CurrentQuestion);
            }

            // Check if the combining of the responses was successful
            if (!screening.CombineResponses()) // Then combine the responses gathered from the user
            {
                await Context.User.SendMessageAsync("Please provide a valid response first");
                return;
            }

            // Check if the screening has not been completed
            if (!screening.ScreeningCompleted)
            {
                string question = ScriptEngineManager.scriptEngine.GetQuestion(screening.CurrentQuestion, ScreeningManager.moduleLocation);
                screening.AddQuestion(question);
                //Console.WriteLine("Question(" + screening.CurrentQuestion + "): " + question);
                await Context.User.SendMessageAsync((screening.CurrentQuestion) + ". " + question);
            }
            else // Otherwise send message saying screening has been completed
            {
                var client = Context.Client;
                //await ((SocketTextChannel)Context.Client.GetChannel(418925364400881685)).SendFileAsync(".//" + screening.UserName + "_screening.txt", "@everyone, screening for " + Context.User.Mention);

                ScreeningManager.screenMngr.PostScreening(Context, ScreeningManager.screenMngr.screeningsChannel, ".//Completed Screenings//" + screening.UserName + "_screening.txt", "@everyone, screening for " + Context.User.Mention);
                Console.WriteLine("User " + screening.FullUserName + " has completed their screening");
                ScreeningManager.screenMngr.RemoveScreening(screening);

                await Context.User.SendMessageAsync(ScreeningManager.screenMngr.screenCompleteMessage);
            }
        }

        /// <summary>
        /// Bot command to skip a question if the user chooses so
        /// </summary>
        /// <returns></returns>
        [Command("abstain")]
        public async Task AbstainQuestion([Summary("The reason as to why the particular question is being skipped"), Remainder] string reason)
        {
            ScreeningProcess screening = ScreeningManager.screenMngr.GetScreening(Context.User.Id); // Get the users screening

            // Check if the screening has been begun
            if (screening == null)
            {
                await Context.User.SendMessageAsync("You have not begun your screening. Please type in '?begin' to start your screening");
                return;
            }

            // Check if the user's screening has been completed
            if (screening.ScreeningCompleted)
            {
                await Context.User.SendMessageAsync("You have already answered all of the questions");
                return;
            }

            Console.WriteLine("User " + Context.User.Username + "#" + Context.User.Discriminator + " has abstained from question " + screening.CurrentQuestion);

            screening.AddResponse("User abstained\n" + "Reason: " + ((reason == string.Empty) ? "none specified" : reason));
            screening.CombineResponses();


            await Context.User.SendMessageAsync("You have chosen to abstain from this question. Next question");

            if (!screening.ScreeningCompleted)
            {
                string question = ScriptEngineManager.scriptEngine.GetQuestion(screening.CurrentQuestion, ScreeningManager.moduleLocation);
                screening.AddQuestion(question);
                await Context.User.SendMessageAsync((screening.CurrentQuestion) + ". " + question);
            }
        }

        /// <summary>
        /// Bot command to stop screening prcess and continue later (TODO: Implement this)
        /// </summary>
        [Command("save")]
        public async Task ScreenLater()
        {
            ScreeningProcess screening = ScreeningManager.screenMngr.GetScreening(Context.User.Id); // Get the user's screening

            // Check if the user has begun their screening
            if (screening == null)
            {
                await Context.User.SendMessageAsync("You have not begun your screening. Please type in '?begin' to start your screening");
                return;
            }

            // Check if the user has begun their screening
            if (screening != null)
            {
                // Check if the user has completed their screening
                if (screening.ScreeningCompleted)
                {
                    await Context.User.SendMessageAsync("You have already completed your screening"); // Send message to user
                    return;
                }
                else
                {
                    // Combine what ever responses they may have sent for the message
                    if (screening.CombineResponses())
                    {
                        Console.WriteLine("User " + screening.FullUserName + " current responses saved");
                    }

                    // Then save the screenings to a file
                    screening.SaveScreening();

                    // If the screening has been completed (when saved) then send the message that the user has finished
                    if (screening.ScreeningCompleted)
                    {
                        var client = Context.Client;
                        //await ((SocketTextChannel)Context.Client.GetChannel(418925364400881685)).SendFileAsync(".//" + screening.UserName + "_screening.txt", "@everyone, screening for " + Context.User.Mention);
                        ScreeningManager.screenMngr.PostScreening(Context, ScreeningManager.screenMngr.screeningsChannel, ".//Completed Screenings//" + screening.UserName + "_screening.txt", "@everyone, screening for " + Context.User.Mention);
                        Console.WriteLine("User " + screening.FullUserName + " has completed their screening");
                        ScreeningManager.screenMngr.RemoveScreening(screening);

                        await Context.User.SendMessageAsync(ScreeningManager.screenMngr.screenCompleteMessage);
                    }
                    else
                    {
                        // And remove from current screenings
                        ScreeningManager.screenMngr.RemoveScreening(screening);
                        await Context.User.SendMessageAsync("Your screening has been saved"); /*, You have 72 hours to complete your screening (WIP)"); // Let the user know their screening has been saved*/
                    }

                    Console.WriteLine(Context.User.Username + "#" + Context.User.Discriminator + " issued save command");
                }
            }
        }

        /// <summary>
        /// Bot command for when the user resumes a previously saved screening
        /// </summary>
        /// <returns></returns>
        [Command("continue")]
        public async Task ContinueScreening()
        {
            ScreeningProcess newScreening = new ScreeningProcess(Context.User.Username, "#" + Context.User.Discriminator, Context.User.Id);
            bool previousDataFound = ScreeningManager.screenMngr.LoadScreening(ref newScreening);

            if (!previousDataFound)
            {
                Console.WriteLine("No previous screening data for user " + Context.User.Username + "#" + Context.User.Discriminator + " to load");
                await Context.User.SendMessageAsync("No previous screening data found.\n" +
                    "Please start a new screening instead by typing '?begin'");
                return;
            }
            else
            {
                Console.WriteLine("Loaded data for user " + Context.User.Username + "#" + Context.User.Discriminator);
            }

            await Context.User.SendMessageAsync(newScreening.CurrentQuestion + ". " + newScreening.CurrentChosenQuestion);
        }

        [Command("help")]
        public async Task Help()
        {
            EmbedBuilder builder = new EmbedBuilder();

            builder.WithTitle("Interrogation Bot Commands");
            builder.AddInlineField("?begin", "Begins the screening, if not already begun.");
            builder.AddInlineField("?next", "Moves onto the next question, once an adequate response is provided.");
            builder.AddInlineField("?abstain", "If you, for some reason, cannot answer a question then you may sakip the question. You must also provide a reason for doing so (Format is \"?abstain <Reason>\")");
            builder.AddInlineField("?save", "Saves the screening for later.");
            builder.AddInlineField("?continue", "Resumes the saved screening, if you have saved a screening previously.");
            builder.AddInlineField("?restart", "Clears the current screening and starts from question 1 (Be sure you want to restarting before doing so)");

            Console.WriteLine(Context.User + "#" + Context.User.Discriminator + " issued help command");
            //await Context.User.SendMessageAsync("Here is a list of commands that you can use\n" +
            //    "```?begin: begins the screening, if not already begun.\n" +
            //    "?next: Moves onto the next question, once an adequate response is provided.\n" +
            //    "?abstain: If you, for some reason, cannot answer a question then you may sakip the question. You must also provide a reason for doing so (Format is \"?abstain <Reason>\")\n" +
            //    "?save: Saves the screening for later.\n" +
            //    "?continue: Resumes the saved screening (If there is one).```");

            await Context.User.SendMessageAsync("", false, builder);
        }


        [Command("restart")]
        public async Task Restart()
        {
            Console.WriteLine(Context.User + "#" + Context.User.Discriminator + " issued restart command");

            // Get the user's screening
            ScreeningProcess screening = ScreeningManager.screenMngr.GetScreening(Context.User.Id);

            // If the screening returns null
            // then the user has not begun their screening
            if(screening == null)
            {
                await Context.User.SendMessageAsync("No screening found to clear");
            }

            // If there is a screening to clear
            if (screening != null)
            {
                screening.Clear();
                await Context.User.SendMessageAsync("Screening has been cleared, starting again");

                // If the screening has not been completed
                if (!screening.ScreeningCompleted)
                {
                    // Get the new question and post it again
                    string question = ScriptEngineManager.scriptEngine.GetQuestion(screening.CurrentQuestion, ScreeningManager.moduleLocation);
                    screening.AddQuestion(question);
                    await Context.User.SendMessageAsync((screening.CurrentQuestion) + ". " + question);
                }
            }
        }

#if DEBUG
        [Command("test")]
        public async Task Test()
        {
            await ReplyAsync(ScreeningManager.screenMngr.userJoinMessage);
        }
#endif
    }
}